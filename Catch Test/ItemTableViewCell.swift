//
//  ItemTableViewCell.swift
//  Catch Test
//
//  Created by Crunchie on 1/08/17.
//  Copyright © 2017 Luke Job. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(item: CatchItem){
        titleLabel.text = item.title
        subtitleLabel.text = item.subtitle
        
        self.backgroundColor = UIColor(white: 1.0, alpha: 0.8)
    }
    
}

extension UITableViewCell {
    func prepareDisclosureIndicator() {
        for case let button as UIButton in subviews {
            let image = button.backgroundImage(for: .normal)?.withRenderingMode(.alwaysTemplate)
            button.setBackgroundImage(image, for: .normal)
            //fixes inability to set tint colour of chevron accessory on cell
        }
    }
}
