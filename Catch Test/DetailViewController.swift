//
//  DetailViewController.swift
//  Catch Test
//
//  Created by Crunchie on 1/08/17.
//  Copyright © 2017 Luke Job. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var contentTextView: UITextView!
    var item:CatchItem?

    override func viewDidLoad() {
        super.viewDidLoad()
        if let item = item{
            self.title = item.title
            contentTextView.text = item.content
            
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        contentTextView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)  //ensure content is scrolled to top when view loads
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

}
