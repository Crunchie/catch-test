//
//  CatchItem.swift
//  Catch Test
//
//  Created by Crunchie on 1/08/17.
//  Copyright © 2017 Luke Job. All rights reserved.
//

import SwiftyJSON

struct CatchItem {
    
    let Id: Int
    let title: String?
    let subtitle: String?
    let content: String?
    
    init?(json: JSON) {
        guard let Id = json["id"].int else {
            return nil
        }
        self.Id = Id
        title = json["title"].string
        subtitle = json["subtitle"].string
        content = json["content"].string
    }
    
}
