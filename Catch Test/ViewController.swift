//
//  ViewController.swift
//  Catch Test
//
//  Created by Crunchie on 1/08/17.
//  Copyright © 2017 Luke Job. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var items = [CatchItem]()
    let ItemTableViewCellID = "ItemTableViewCellID"
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(ViewController.refresh), for: .valueChanged)
        refreshControl.tintColor = UIColor.white
        return refreshControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.addSubview(self.refreshControl)
        
        tableView.register(UINib(nibName:"ItemTableViewCell", bundle: nil), forCellReuseIdentifier: ItemTableViewCellID)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        tableView.backgroundColor = UIColor.black
        let catchImageView = UIImageView(image: UIImage(named: "catch-logo"))
        catchImageView.contentMode = .center
        tableView.backgroundView = catchImageView
        
        tableView.separatorColor = UIColor(white: 0.7, alpha: 1.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func refresh() {
        Alamofire.request("https://raw.githubusercontent.com/catchnz/ios-test/master/data/data.json", method: .get).validate().responseJSON { response in
            switch response.result {
            case .success:
                let json = JSON(data: response.data!)
                self.loadData(json: json)
                self.tableView.reloadData()
            case .failure(let error):
                print(error)
            }
            self.refreshControl.endRefreshing()
        }
    }
    
    func loadData(json: JSON){
        if let jsonArray = json.array{
            for jsonItem in jsonArray {
                if let item = CatchItem(json: jsonItem){
                    items.append(item)
                }
            }
        }
    }
    
    // MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell:ItemTableViewCell = tableView.dequeueReusableCell(withIdentifier: ItemTableViewCellID) as! ItemTableViewCell
        cell.setup(item: items[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.prepareDisclosureIndicator()
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        tableView.deselectRow(at: indexPath, animated: true)
        let detailViewController = storyboard!.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        detailViewController.item = items[indexPath.row]
        navigationController?.pushViewController(detailViewController, animated: true)
    }

}

