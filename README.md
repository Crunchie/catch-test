# Catch Test iOS

## Requirements

- Xcode >= 8.3.3
- CocoaPods >= 1.2.1

## Setup

- Ensure requirements above are installed
- Clone repository
- Install the pods
- Open "Catch Test.xcworkspace"
- Run app
