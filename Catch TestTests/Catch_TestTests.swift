//
//  Catch_TestTests.swift
//  Catch TestTests
//
//  Created by Crunchie on 1/08/17.
//  Copyright © 2017 Luke Job. All rights reserved.
//

import XCTest
import SwiftyJSON
@testable import Catch_Test

class Catch_TestTests: XCTestCase {
    
    var viewController: ViewController?
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main",
                                      bundle: Bundle.main)
        let navigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        viewController = navigationController.topViewController as? ViewController
        
        UIApplication.shared.keyWindow!.rootViewController = viewController
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testJsonDataLoading() {
        //normal data
        let testJson = "[{\"id\":1,\"title\":\"nonummy integer\",\"subtitle\":\"sapien arcu\",\"content\":\"Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue.\"},{\"id\":2,\"title\":\"in est risus\",\"subtitle\":\"nec euismod\",\"content\":\"Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo.\"},{\"id\":3,\"title\":\"etiam\",\"subtitle\":\"mauris enim leo rhoncus\",\"content\":\"Proin interdum mauris non ligula pellentesque ultrices.\"},{\"id\":4,\"title\":\"turpis integer aliquet\",\"subtitle\":\"cursus vestibulum proin eu\",\"content\":\"Aliquam sit amet diam in magna bibendum imperdiet.\"}]"
        let data = testJson.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        let json = JSON(data: data!)
        viewController?.loadData(json: json)
        
        assert(viewController?.items.count == 4)
    }
    
    func testJsonDataLoading1() {
        //missing 2 IDs (replaced with empty quotes)
        let testJson = "[{\"id\":\"\",\"title\":\"nonummy integer\",\"subtitle\":\"sapien arcu\",\"content\":\"Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue.\"},{\"id\":2,\"title\":\"in est risus\",\"subtitle\":\"nec euismod\",\"content\":\"Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo.\"},{\"id\":\"\",\"title\":\"etiam\",\"subtitle\":\"mauris enim leo rhoncus\",\"content\":\"Proin interdum mauris non ligula pellentesque ultrices.\"},{\"id\":4,\"title\":\"turpis integer aliquet\",\"subtitle\":\"cursus vestibulum proin eu\",\"content\":\"Aliquam sit amet diam in magna bibendum imperdiet.\"}]"
        let data = testJson.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        let json = JSON(data: data!)
        viewController?.loadData(json: json)
        
        assert(viewController?.items.count == 2)
    }
    
    func testJsonDataLoading2() {
        //missing 2 titles
        let testJson = "[{\"id\":1,\"title\":\"\",\"subtitle\":\"sapien arcu\",\"content\":\"Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue.\"},{\"id\":2,\"title\":\"in est risus\",\"subtitle\":\"nec euismod\",\"content\":\"Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo.\"},{\"id\":3,\"title\":\"etiam\",\"subtitle\":\"mauris enim leo rhoncus\",\"content\":\"Proin interdum mauris non ligula pellentesque ultrices.\"},{\"id\":4,\"title\":\"\",\"subtitle\":\"cursus vestibulum proin eu\",\"content\":\"Aliquam sit amet diam in magna bibendum imperdiet.\"}]"
        let data = testJson.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        let json = JSON(data: data!)
        viewController?.loadData(json: json)
        
        assert(viewController?.items.count == 4)
    }
    
    func testJsonDataLoading3() {
        //missing all but IDs
        let testJson = "[{\"id\":1,\"title\":\"\",\"subtitle\":\"\",\"content\":\"\"},{\"id\":2,\"title\":\"\",\"subtitle\":\"\",\"content\":\"\"},{\"id\":3,\"title\":\"\",\"subtitle\":\"\",\"content\":\"\"},{\"id\":4,\"title\":\"\",\"subtitle\":\"\",\"content\":\"\"}]"
        let data = testJson.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        let json = JSON(data: data!)
        viewController?.loadData(json: json)
        
        assert(viewController?.items.count == 4)
    }
    
}
